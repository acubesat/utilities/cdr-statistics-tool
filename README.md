**OMIGI finally.**

The tool that can provide statistics related to the CDR document list.
- Do you wanna know how many pages or words did the CDR have?
- Do you wanna know which subsystem contributed more?
- Do you wanna know *NUMBERS*? 

Here is da place. Enjoy.

![Screenshot of a wordcloud generated with wordcloud_cli](screenshot.png)

## Usage

- **`./latex-wordcount.py`** `/cdr/*.pdf`:
