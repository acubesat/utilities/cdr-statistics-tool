set terminal svg size 1920, 400
set output 'date-plot.svg'

set title font "Noto Sans,20"
set title "Lines changed/day"
set xdata time
#with filledcurves x1
set timefmt "%Y-%m-%d"
#set xrange ["03/21/95":"03/22/95"]
set format x "%d %B"
#set timefmt "%m/%d/%y %H:%M"
set tics font "Noto Sans,15"
set grid
plot "lines-date.dat" using 1:2 smooth unique lw 4, "lines-date.dat" using 1:2 smooth bezier w l lc rgb "#77ffaa00" lw 3 title "Average"
set style line 1 lw 30
