#!/usr/bin/env bash

# Statistics script
# =================
# Total numbers of figures and tables in the script

REPO_DIRECTORY=${REPO_DIRECTORY:-$HOME/repos/cdr}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

figures=$(find $REPO_DIRECTORY \( -not -path $REPO_DIRECTORY/utils/\* \) -and \( -not -iname '*template*' \) -and \( -not -iname '*extra*' \) -and \( -not -iname '*index*' \) -and -name "*.tex" -exec grep "\\\\begin{figure}" "{}" \; | grep -v \% | wc -l)
tables=$(find $REPO_DIRECTORY \( -not -path $REPO_DIRECTORY/utils/\* \) -and \( -not -iname '*template*' \) -and \( -not -iname '*extra*' \) -and \( -not -iname '*index*' \) -and -name "*.tex" -exec grep "\\\\begin{table}" "{}" \; | grep -v \% | wc -l)
longtables=$(find $REPO_DIRECTORY \( -not -path $REPO_DIRECTORY/utils/\* \) -and \( -not -iname '*template*' \) -and \( -not -iname '*extra*' \) -and \( -not -iname '*index*' \) -and -name "*.tex" -exec grep "\\\\begin{longtable}" "{}" \; | grep -v \% | wc -l)
totaltables=$(expr $tables + $longtables)

echo "Figures $figures"
echo "Tables $totaltables"
