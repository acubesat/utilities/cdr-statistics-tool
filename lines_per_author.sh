#!/usr/bin/env bash

# Statistics script
# =================
# Number of pages in every PDF document

REPO_DIRECTORY=${REPO_DIRECTORY:-$HOME/repos/cdr}

declare -A usernames_authors
declare -A authors_num_of_lines

usernames_authors_file=$(cat author-names.txt)
while read line
do
    username=$(echo $line | awk -F ',' '{print $1}')
    full_name=$(echo $line | awk -F ',' '{print $2}')

    usernames_authors["$username"]="$full_name"
done < author-names.txt

cd $REPO_DIRECTORY

USERNAMES="$(git shortlog -s | cut -c8-)"

while read username
do
    SUM=$(git log --author="^$username" --pretty=tformat: --numstat '*.tex' | awk '$1 + $2 {sum += $1 + $2} END {print sum}')

    if [[ -z $SUM ]]
    then
        SUM=0
    fi

    if [ -v usernames_authors["$username"] ]
    then
        full_name="${usernames_authors[$username]}"
    else
        full_name="$username"
    fi

    authors_num_of_lines["$full_name"]=`expr ${authors_num_of_lines["$full_name"]} + $SUM`
done <<< "$USERNAMES"

for i in "${!authors_num_of_lines[@]}"
do
  echo -en "${authors_num_of_lines[$i]} \t"
  echo "$i"
done | sort -gr
