#!/usr/bin/env bash

# Statistics script
# =================
# Number of pages in every PDF document

REPO_DIRECTORY=${REPO_DIRECTORY:-$HOME/repos/cdr}

cd $REPO_DIRECTORY

USERNAMES="$(git shortlog -s | cut -c8-)"

START_DATE="2020-08-01"
DAYS=66

CURRENT_DATE=$START_DATE
for i in $(seq 0 $DAYS)
do
    DATE=`date -d "$START_DATE + $i days" -I`

    SUM=$(git log --after="$DATE 00:00" --before="$DATE 24:00" --pretty=tformat: --numstat '*.tex' | awk '$1 + $2 {sum += $1 + $2} END {print sum}')

    if [[ -z $SUM ]]
    then
        SUM=0
    fi

    echo "$DATE $SUM"
done
