from termgraph import termgraph
from git import Repo
import os
import time
from datetime import datetime
from dateutil import tz
import difflib
from tqdm import tqdm

repo = Repo(os.environ.get('REPO_DIRECTORY') or "~/repos/cdr")

print(repo)

hours = {h: 0 for h in range(0,24)}

local_timezone = tz.tzlocal()

commits = list(repo.iter_commits())

for commit in tqdm(commits):
    date = datetime.fromtimestamp(commit.authored_date).replace(tzinfo=tz.tzoffset(None, commit.author_tz_offset))
    date = date.astimezone(local_timezone)

    output = repo.git.show("--shortstat", commit.hexsha, "--pretty=tformat:", "*.tex").split(' ')

    insertions = output[4] if len(output) > 4 else 0
    deletions = output[6] if len(output) > 6 else 0

    hours[date.hour] += int(insertions) + int(deletions)



with open('lines_per_hour.dat', 'w') as file:
    for hour in hours.items():
        file.write("%d %d\n" % hour)
