#!/usr/bin/env bash

# Statistics script
# =================
# Number of words in one document

REPO_DIRECTORY=${REPO_DIRECTORY:-$HOME/repos/cdr}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

texfiles=`find $REPO_DIRECTORY \( -not -path $REPO_DIRECTORY/utils/\* \) -and \( -not -iname '*template*' \) -and \( -not -iname '*extra*' \) -and \( -not -iname '*index*' \) -and -name "*.tex"`

ALLWORDS=""

while read s
do
    WORDS="`./extra_utilities/latex-wordcount.py "$s" | head -n-1`"
    ALLWORDS+="$WORDS"
done <<< "$texfiles"

echo "$ALLWORDS" | sort | uniq -c | sort -g