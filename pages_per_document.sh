#!/usr/bin/env bash

# Statistics script
# =================
# Pages per CDR PDF document

REPO_DIRECTORY=${REPO_DIRECTORY:-$HOME/repos/cdr}

cd $REPO_DIRECTORY/build/output

exiftool -T -filename -PageCount -s3 -ext pdf . | grep -v test | grep -v 'index.pdf' | grep -v 'template.pdf' | awk '{print $2, $1}' | sort -gr