#!/usr/bin/env bash

# Statistics script
# =================
# Total numbers of pages in all PDF documents

./pages_per_document.sh | awk '{ s += $1; } END { print s }'
