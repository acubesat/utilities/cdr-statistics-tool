#!/usr/bin/env bash

# Statistics script
# =================
# Create a word cloud based on the most frequent words
# Note: Needs you to run `pip install wordcloud`

REPO_DIRECTORY=${REPO_DIRECTORY:-$HOME/repos/cdr}

texfiles=`find $REPO_DIRECTORY \( -not -path $REPO_DIRECTORY/utils/\* \) -and \( -not -iname '*template*' \) -and \( -not -iname '*extra*' \) -and \( -not -iname '*index*' \) -and -name "*.tex"`

ALLWORDS=""

while read s
do
    WORDS="`./extra_utilities/latex-wordcount.py "$s" | head -n-1`"
    ALLWORDS+="$WORDS"
done <<< "$texfiles"

echo "$ALLWORDS" | wordcloud_cli --imagefile wordcloud.png --relative_scaling 0.2 --background white --include_numbers --scale 5 --stopwords /dev/null --colormap turbo