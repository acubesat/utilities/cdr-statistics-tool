#!/usr/bin/env bash

# Statistics script
# =================
# Total numbers of words in all PDF documents

./words_per_document.sh | awk '{ s += $1; } END { print s }'
